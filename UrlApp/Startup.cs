﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using UrlApp.Dal.Context;
using AutoMapper;
using UrlApp.BL.Contracts;
using UrlApp.BL.Services;
using UrlApp.Dal.Contracts;
using UrlApp.Dal.Repositories;

namespace UrlApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IUrlRepository, UrlRepository>();
            services.AddTransient<IUrlService, UrlService>();

            var connectionString = this.Configuration["connectionStrings:DefaultConnection"];
            services.AddDbContext<EFContext>(o => o.UseSqlServer(connectionString).EnableSensitiveDataLogging());

            services.AddAutoMapper();
            services.AddMvc();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, EFContext context)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            context.EnsureSeedDataForContext();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "recognizeUrl",
                    template: "{controller=Home}/{action=RecognizeUrl}/{shortcut?}");
            });
        }
    }
}
