using System;

namespace UrlApp.Models
{
    public class ErrorViewModel
    {
        public string Error { get; set; }
    }
}