﻿var app = angular.module("urlApp", []);

app.controller("urlCtrl", function ($scope, $http) {

    var apiRoute = '/urls';

    $scope.Insert = function () {

        var action = document.getElementById("btnSave").getAttribute("value");

        if (action == "Submit") {

            //create
            $scope.UrlObject = {};
            $scope.UrlObject.BaseUrl = $scope.BaseUrl;
            $scope.UrlObject.Shortcut = $scope.Shortcut;

            $http({
                method: "post",
                url: apiRoute,
                datatype: "json",
                data: JSON.stringify($scope.UrlObject)
            }).then(function (response) {
                $scope.GetAll();

                $scope.BaseUrl = "";
                $scope.Shortcut = "";
            }, function () {
                alert("Creation Error");
            })
        } else {

            // update
            $scope.UrlObject = {};
            $scope.UrlObject.BaseUrl = $scope.BaseUrl;
            $scope.UrlObject.Shortcut = $scope.Shortcut;
            $scope.UrlObject.ID = document.getElementById("UrlID").value;

            $http({
                method: "put",
                url: apiRoute,
                datatype: "json",
                data: JSON.stringify($scope.UrlObject)
            }).then(function (response) {
                $scope.GetAll();

                $scope.BaseUrl = "";
                $scope.Shortcut = "";

                document.getElementById("btnSave").setAttribute("value", "Submit");
                document.getElementById("btnSave").style.backgroundColor = "cornflowerblue";
                document.getElementById("spn").innerHTML = "Add New Url";
            }, function () {
                alert("Update Error");
            })
        }
    };

    $scope.GetAll = function () {

        $http({
            method: "get",
            url: apiRoute
        }).then(function (response) {
            $scope.urls = response.data;
        }, function () {
            alert("Error getting list");
        })
    };

    $scope.Delete = function (url) {
        $http({
            method: "delete",
            url: apiRoute + "/" + url.id

        }).then(function (response) {
            $scope.GetAll();

        }, function () {
            alert("Error deleting");
        })
    };

    $scope.Update = function (url) {

        document.getElementById("UrlID").value = url.id;
        $scope.BaseUrl = url.baseUrl;
        $scope.Shortcut = url.shortcut;

        document.getElementById("btnSave").setAttribute("value", "Update");
        document.getElementById("btnSave").style.backgroundColor = "Yellow";
        document.getElementById("spn").innerHTML = "Update Url";
    };
});