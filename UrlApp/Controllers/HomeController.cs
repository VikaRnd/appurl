﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UrlApp.BL.Contracts;
using UrlApp.Models;

namespace UrlApp.Controllers
{
    public class HomeController : Controller
    {
        private IUrlService _urlService;

        public HomeController(IUrlService urlService)
        {
            _urlService = urlService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [Route("{shortcut}")]
        public IActionResult RecognizeUrl(string shortcut)
        {
            var urlViewModel = _urlService.Get(u => u.Shortcut == shortcut);

            if (urlViewModel != null)
            {
                return Redirect(urlViewModel.BaseUrl);
            }
            else
            {
                ViewBag.Error = "The shortcut does not exist.";
                return View("Error");
            }
        }
    }
}
