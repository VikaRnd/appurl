﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using UrlApp.BL.Contracts;
using UrlApp.BL.ViewModels;

namespace UrlApp.Controllers
{
    [Route("urls")]
    public class UrlController : Controller
    {
        private IUrlService _urlService;

        public UrlController(IUrlService urlService)
        {
            _urlService = urlService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var urls = _urlService.GetAll();

                if (urls == null)
                {
                    return NotFound();
                }

                return Ok(urls);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody]UrlViewModel model)
        {
            if (model == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var newModelId = _urlService.Insert(model);

                if (newModelId > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPut]
        public IActionResult Put([FromBody]UrlViewModel model)
        {
            if (model == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                _urlService.Update(model);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

            return Ok();
        }

        [HttpDelete("{id:int}")]
        public IActionResult Delete(int id)
        {
            try
            {
                if (id > 0)
                {
                    _urlService.Delete(id);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

            return Ok();
        }
    }
}