﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using UrlApp.Dal.Context;
using UrlApp.Dal.Contracts;
using UrlApp.Dal.Entities;

namespace UrlApp.Dal.Repositories
{
    public class UrlRepository : IUrlRepository
    {
        private EFContext _context;

        public UrlRepository(EFContext context)
        {
            _context = context;
        }

        protected DbSet<Url> DbSet
        {
            get { return _context.Urls; }
        }

        public List<Url> GetAll()
        {
            return DbSet.ToList();
        }

        public Url Get(Expression<Func<Url, bool>> filter = null)
        {
            return DbSet.AsNoTracking().FirstOrDefault(filter);
        }

        public void Delete(int id)
        {
            Url entity = DbSet.Find(id);
            if (_context.Entry(entity).State == EntityState.Detached)
            {
                DbSet.Attach(entity);
            }
            _context.Entry(entity).State = EntityState.Deleted;
            _context.SaveChanges();
        }

        public int Insert(Url entity)
        {
            DbSet.Add(entity);
            _context.SaveChanges();

            return entity.ID;
        }

        public void Update(Url entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            _context.SaveChanges();
        }
    }
}
