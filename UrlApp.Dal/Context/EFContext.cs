﻿using Microsoft.EntityFrameworkCore;
using UrlApp.Dal.Entities;

namespace UrlApp.Dal.Context
{
    public class EFContext : DbContext
    {
        public EFContext(DbContextOptions<EFContext> options) : base (options)
        {
            Database.EnsureCreated();
        }

        public DbSet<Url> Urls { get; set; }
    }
}
