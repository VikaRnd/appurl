﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UrlApp.Dal.Entities;

namespace UrlApp.Dal.Context
{
    public static class EFContextExtension
    {
        public static void EnsureSeedDataForContext(this EFContext context)
        {
            if (context.Urls.Any())
                return;

            var urlData = new List<Url>()
            {
                new Url()
                {
                    BaseUrl = "http://google.com",
                    Shortcut = "ggl"
                },
                new Url()
                {
                    BaseUrl = "http://cnn.com",
                    Shortcut = "cnn"
                },
            };

            context.AddRange(urlData);
            context.SaveChanges();
        }
    }
}
