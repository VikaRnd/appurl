﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using UrlApp.Dal.Entities;

namespace UrlApp.Dal.Contracts
{
    public interface IUrlRepository
    {
        List<Url> GetAll();
        Url Get(Expression<Func<Url, bool>> filter = null);
        int Insert(Url entity);
        void Update(Url entity);
        void Delete(int id);
    }
}
