﻿using System.ComponentModel.DataAnnotations;

namespace UrlApp.BL.ViewModels
{
    public class UrlViewModel
    {
        public int ID { get; set; }

        [Required]
        [Url]
        public string BaseUrl { get; set; }

        [Required]
        public string Shortcut { get; set; }
    }
}
