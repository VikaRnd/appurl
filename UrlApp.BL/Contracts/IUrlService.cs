﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using UrlApp.BL.ViewModels;

namespace UrlApp.BL.Contracts
{
    public interface IUrlService
    {
        List<UrlViewModel> GetAll();
        UrlViewModel Get(Expression<Func<UrlViewModel, bool>> filter = null);
        int Insert(UrlViewModel entityVM);
        void Update(UrlViewModel entityVM);
        void Delete(int id);
    }
}
