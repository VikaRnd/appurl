﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using UrlApp.BL.Contracts;
using UrlApp.BL.ViewModels;
using UrlApp.Dal.Contracts;
using UrlApp.Dal.Entities;

namespace UrlApp.BL.Services
{
    public class UrlService : IUrlService
    {
        IUrlRepository _repository;
        IMapper _mapper;

        public UrlService(IUrlRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public void Delete(int id)
        {
            _repository.Delete(id);
        }

        public UrlViewModel Get(Expression<Func<UrlViewModel, bool>> filter = null)
        {
            var mappedFilter = _mapper.Map<Expression<Func<Url, bool>>>(filter);
            var entity = _repository.Get(mappedFilter);
            var entityVM = _mapper.Map<UrlViewModel>(entity);

            return entityVM;
        }

        public List<UrlViewModel> GetAll()
        {
            var entityListDal = _repository.GetAll();
            var entityListVM = _mapper.Map<List<UrlViewModel>>(entityListDal);
            return entityListVM;
        }

        public int Insert(UrlViewModel entity)
        {
            var entityDal = _mapper.Map<Url>(entity);
            int newEntityId = _repository.Insert(entityDal);
            return newEntityId;
        }

        public void Update(UrlViewModel entity)
        {
            var entityDal = _mapper.Map<Url>(entity);
            _repository.Update(entityDal);
        }
    }
}
