﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UrlApp.BL.ViewModels;
using UrlApp.Dal.Entities;

namespace UrlApp.BL.Mappers
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<UrlViewModel, Url>();
            CreateMap<Url, UrlViewModel>();
        }
    }
}
